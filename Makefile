LOCATION=`kde4-config --install data`/kdevanalitzachecks

check: *.kdevcheck
	~/build-pfc/kdevanalitzachecks/checker/kdevanalitzacheckscompiler $?

install:
	mkdir -p ${LOCATION}
	install -v *.kal *kdevcheck ${LOCATION}

uninstall:
	rm -rf ${LOCATION}/*.kal ${LOCATION}/*.kdevcheck
